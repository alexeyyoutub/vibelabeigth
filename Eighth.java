import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Eighth {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<>();

        map.put(1111, "Myau");
        map.put(2222, "Vuf");
        map.put(3333, "AinSvain");
        map.put(4444, "Hehe");
        Set<Integer> keys = map.keySet();
        System.out.println("All keys:\n" + keys);

        System.out.println("\nSize of HashMap: " + map.size());

        map.remove(4444);
        map.remove(3333);
        ArrayList <String> values = new ArrayList<>(map.values());
        System.out.println("\nValues after removing:\n" + values);

        System.out.println("\nInfo of key 1111: " + map.containsKey(1111));
        System.out.println("Info of value Hehe: " + map.containsValue("Hehe"));

        System.out.println("\nChecking of .get: " + map.get(1111));

        map.clear();

        System.out.println("\nChecking for emptiness after cleaning: " + map.isEmpty());
    }
}
